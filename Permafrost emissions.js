
// Àrea de càlcul
var geom = regio.buffer(1500);

Map.addLayer(regio);
Map.addLayer(geom);


var ch4 = ee.ImageCollection("COPERNICUS/S5P/OFFL/L3_CH4")
  .select('CH4_column_volume_mixing_ratio_dry_air')
  // Les dades comencen a estar disponibles el 22-3-19 per al cràter de Batagaika.
  .filterDate('2019-02-01','2019-12-31')
  .filterBounds(regio)
  
print("Resultats: ", ch4.size())


// Color
var visualitzacio = {
  min: 1750,
  max: 1900,
  palette: ['black', 'blue', 'purple', 'cyan', 'green', 'yellow', 'red']
};

// Propietats de la col·lecció i addició al mapa
print(ch4.first().getInfo());
Map.addLayer(ch4.mean(), visualitzacio, 'Sentinel 5P CH4');

// Generació d'una gràfica. Què és l'escala?
var grafica = ui.Chart.image.series(ch4, geom, ee.Reducer.mean(), 50)
  .setSeriesNames(['Gas metà'])
  .setOptions({
    title: 'Nivells de metà CH4 - Sentinel 5-P',
    interpolateNulls: true,
    lineWidth: 1,
    pointSize: 3,
    vAxis: {title: 'Col·lumna de metà (mol/m²?)'},
    hAxis: {title: 'Data', format: 'dd MMM YYYY', gridlines: {count: 12}}
  });

print(grafica);
