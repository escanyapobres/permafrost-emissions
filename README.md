# Permafrost emissions (English)
## 1. Description
Just a short experiment to indentify methane emissions on Batagaika crater, Siberia.

## 2. Context and importance
Melting permafrost in the boreal regions of the planet can significantly increase levels of greenhouse gases (GHGs) in the atmosphere. Although the amount of GHG stored in the permafrost is very uncertain it is also known to be considerable. We propose this attempt to  accurately quantify CH4 methane emissions in the region.

## 3. Place of study
The area chosen is Batagaika Crater in the Sakha Republic, Russian Federation. This crater was formed by the melted subsurface permafrost, this crater is currently the largest one of this type in the world.

![alt text](https://upload.wikimedia.org/wikipedia/commons/d/da/Batagaika_crater_NASA.jpg "Batagaika crater")

## 2. Tools and information
- Sentinel 5-P images: Gives $`CH_4`$ levels in offline (OFFL) mode, available from February 2019. [Processed by Google](https://developers.google.com/earth-engine/datasets/catalog/COPERNICUS_S5P_OFFL_L3_CH4#description) using Harp with quality parameter greater than 0.5. 
- Google Earth Engine: Process information.
- LibreOffice Calc: Plotting and statistics.

## 3. Methodology
1. Creating a script (Permafrost emissions.js) from Google Earth Engine to select the data. The calculation area will be a circle with a radius of 1.5 km, with its center in the middle of the crater.

Link to the Google Earth Engine script: https://code.earthengine.google.com/bef6b8fa8c716b4c7460e03048463861

![alt text](/imgs/img1.png "Regió de càlcul")

2. The code will calculate the mean value of $`CH_4`$ for the study region and graph it.
3. We will export the data to .csv format and process the values with LibreOffice Calc. We will then graph it and make a brief statistical analysis.
4. Extract conclusions from the results.

## 4. Results
We did not detect any trend in the $`CH_4`$ emissions on Batagaika crater during the year 2019.
![alt text](/imgs/img2.png "Results year 2019")

## 5. Discussion
Although we have not found any trend in the future we will study this problem from another point of view.


# Permafrost emissions (Valencian/Catalan)
## 1. Descripció
Només un breu experiment per identificar les emissions de metà al crater de Batagaika, Sibèria. 

## 2. Context i importància
La fundició del permafrost a les regions boreals del planeta pot augmentar considerablement els nivells de gassos d'efecte hivernacle (GEH) a l'atmosfera. Tot i que la quantitat de GEH emmagatzemats en el permafrost és molt incerta se sap que és considerable. Proposem este intent per poder quantificar precissament les emissions de metà ($`CH_{4}`$) a la regió.

## 3. Zona d'estudi
La zona escollida és el cràter de Batagaika a la República de Sakha, Federació de Russia. Este cràter s'ha format per la fundició del permafrost del subsòl, actualment és el cràter d'este tipus més gran del món.

![alt text](https://upload.wikimedia.org/wikipedia/commons/d/da/Batagaika_crater_NASA.jpg "Crater de Batagaika")

## 2. Ferramentes i informació utilitzada
- Imatges del Sentinel 5-P: Proporciona els nivells de $`CH_4`$ en mode offline (OFFL), disponibles des de febrer de 2019. [Processat per Google](https://developers.google.com/earth-engine/datasets/catalog/COPERNICUS_S5P_OFFL_L3_CH4#description) utilitzant Harp amb el paràmetre de qualitat major que 0,5. 
- Google Earth Engine: Processar la informació.
- LibreOffice Calc: Graficat i estadística.

## 3. Metodologia
1. Creació d'un script (Permafrost emissions.js) de Google Earth Engine per seleccionar les dades. La zona de càlcul serà d'un cercle d'1,5km de radi, tenint el seu centre enmig del cràter.

Enllaç al codi de Google Earth Engine: https://code.earthengine.google.com/bef6b8fa8c716b4c7460e03048463861

![alt text](/imgs/img1.png "Regió de càlcul")

2. El codi calcularà el valor mitjà de $`CH_4`$ per a la regió d'estudi i el graficarà.
3. Exportarem les dades a format .csv i processarem els valors amb LibreOffice Calc. Posteriorment ho graficarem i farem una breu anàlisi estadística.
4. Extracció de conclusions a partir dels resultats.

## 4. Resultats
No hem detectat cap tendència en les emissions de $`CH_4`$ al cràter de Batagaika al llarg de l'any 2019.
![alt text](/imgs/img2.png "Resultats any 2019")

## 5. Discussió
Tot i no haver trobat cap tendència en el futur estudiarem este problema des d'un altre punt de vista.
